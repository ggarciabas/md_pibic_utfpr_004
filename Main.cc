/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */

#    include "ns3/core-module.h"
#    include "ns3/internet-module.h"
#    include "ns3/random-variable.h"
#    include "ns3/simulator.h"
#    include "ns3/point-to-point-module.h"
#    include "ns3/applications-module.h"
#    include "ns3/flow-monitor-module.h"
#    include "ns3/ptr.h"
#    include "ns3/network-module.h"
#    include "ns3/mobility-module.h"
#    include "ns3/lte-module.h"
#    include "ns3/config-store.h"
#    include "ns3/epc-helper.h"
#    include "ns3/ipv4-global-routing-helper.h"
#    include "ns3/ipv4.h"
#    include "ns3/wifi-module.h"
#    include "ns3/trace-helper.h"
#    include "ns3/netanim-module.h"
#    include "ns3/propagation-loss-model.h"
#    include "ns3/cost231-propagation-loss-model.h"

#include <iostream>     // std::cout
#include <algorithm>    // std::random_shuffle std::find
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
#define TEMPO_LEITURA 3 // Verificar valores
#define TEMPO_ESPERA 1

using namespace ns3;

// Variaveis utilizadas
// Aplicacao
typedef struct Aplicacao
{
      int id;
      int sender;
      int app;
      std::string dataRate;
      int receiver;
      int grauLte;    // Grau de eligibilidade
      double timeTotal;    // Tempo total para a aplicacao ser finalizada
      int grauWlan;    // Grau de eligibilidade
      double bits;
      int status;    // 0 - request, 1 - renegociacao1, 2 - renegociacao2 e 3 - realocacao
      ApplicationContainer onOff;
      ApplicationContainer sink;
      int interface;    // interface onde esta sendo executada a aplicacao. 0 -lte 1 - wlan
      int direcao;    // 0 - Uplink, 1 - Downlink

      /*// default + parameterized constructor
       Aplicacao ( int id, int sender, int app, std::string dataRate,
       int grauLte, int receiver, double timeTotal,
       int grauWlan, double bits, int status,
       ApplicationContainer onOff,
       ApplicationContainer sink, int interface,
       int direcao )
       : id ( id ), sender ( sender ), app ( app ), dataRate (
       dataRate ), grauLte ( grauLte ), receiver (
       receiver ), timeTotal ( timeTotal ), grauWlan (
       grauWlan ), bits ( bits ), status (
       status ), onOff ( onOff ), sink (
       sink ), interface ( interface ), direcao (
       direcao )
       {
       }

       Aplicacao ( int id, int receiver, int sender, int app,
       int status, double bits, int grauLte, int grauWlan,
       std::string dataRate, int direcao )
       : id ( id ), sender ( sender ), app ( app ), status (
       status ), bits ( bits ), grauLte (
       grauLte ), grauWlan ( grauWlan ), dataRate (
       dataRate ), direcao ( direcao )
       {
       }
       */

      // equality comparison. doesn't modify object. therefore const.
      bool operator== ( const Aplicacao& a ) const
      {
	  return ( id == a.id );
      }
} APP;

Ptr < LteHelper > lteHelper;
Ptr < EpcHelper > epcHelper;
Ptr < Node > pgwNode;
NodeContainer ueNode;
NodeContainer enbNode;
Ptr < Node > apNode;
NodeContainer haNode;
MobilityHelper mobilityHelper;
NetDeviceContainer ueDeviceLte;
NetDeviceContainer ueDeviceWlan;
NetDeviceContainer enbDevice;
NetDeviceContainer apDevice;
NetDeviceContainer haDevice;
Ipv4InterfaceContainer ueInterface;
Ipv4InterfaceContainer apInterface;
Ipv4InterfaceContainer haInterface;
int numeroDispositivos;    // numero de dispositivos
int numeroUsuarios;    // valor utilizado para efetuar os calculos relativo ao tempo final da simulacao e a interchegada de chamadas
int quantidadeAplicacoes;    // numero total de aplicacoes
double tempoInterchegada;    // Tempo de interchegada entre as chamadas em segundos
double tempoTotalChamadas;    // Tempo total de chamadas, soma das interchegadas.
uint32_t recursosLteUl = 0;    // Recursos do LTe utilizados - UPLINK
uint32_t recursosLteDl = 0;    // Recursos do Lte utilziados - DOWNLINK
uint32_t recursosWlanUl = 0;    // Recursos do Wlan utilizados - UPLINK
uint32_t recursosWlanDl = 0;    // Recursos do Wlan utilizados - DOWNLINK
uint32_t ocupacaoRedeLteUl = 9175040;    // Ocupacao da rede Lte total - UPLINK
uint32_t ocupacaoRedeLteDl = 9175040;    // Ocupacao da rede Lte total - DOWNLINK
uint32_t ocupacaoRedeWlanUl = 7077888;    // Ocupacao da rede Wlan total - UPLINK
uint32_t ocupacaoRedeWlanDl = 7077888;    // Ocupacao da rede Wlan total - DOWNLINK
int qtChamadasLte = 0;    // Chamadas aceitas no LTe
int qtChamadasWlan = 0;    // Chamadas aceitas no WLAN
int handoverLteWlan = 0;    // handover Lte to Wlan
int handoverWlanLte = 0;    // handover Wlan to LTE
int qtAceitasSemRenegociacaoReal = 0;    // chamadas aceias sem renegociacao do tipo realtime
int qtAceitasComRenegociacaoReal = 0;    // chamadas aceitas com renegociacao do tipo realtime
int qtAceitasSemRenegociacao = 0;    // chamadas aceitas sem renegociacao do tipo non-realtime
int qtAceitasComRenegociacao = 0;    // chamadas aceitas com renegociacao do tipo non-realtime
int qtVoipLte = 0;    // Chamadas voip aceitas no lte
int qtVideoLte = 0;    // Chamadas video aceitas no lte
int qtVideoConfLte = 0;    // Chamadas videoconf aceitas no lte
int qtFtpLte = 0;    // Chamadas ftp aceitas no lte
int qtWwwLte = 0;    // Chamadas www aceitas no lte
int qtVoipWlan = 0;    // Chamadas voip aceitas no wlan
int qtVideoConfWlan = 0;    // Chamadas video conferencia aceitas no wlan
int qtVideoWlan = 0;    // Chamadas video aceitas no wlan
int qtFtpWlan = 0;    // Chamadas ftp aceitas no wlan
int qtWwwWlan = 0;    // Chamadas www aceitas no wlan
int qtRenegociacao1 = 0;    // Quantidade de chamadas que solicitaram renegociacao 1
int qtRenegociacao2 = 0;    // Quantidade de chamadas que solicitaram renegociacao 2
int realocacaoToLte = 0;    // Quantidade de chamadas realocadas do Wlan para o Lte
int realocacaoToWlan = 0;    // Quantidade de chamadas realocadas do Lte para o Wlan
double tempoTotalWlanUl = 0;    // Tempo total no meio wlan das aplicacoes - DOWNLINK
double tempoTotalWlanDl = 0;    // Tempo total no meio wlan das aplicacoes - DOWNLINK
double pacoteTotalWlanUl = 0;    // Total de pacotes no meio wlan das aplicacoes (size) - UPLINK
double pacoteTotalWlanDl = 0;    // Total de pacotes no meio wlan das aplicacoes (size) - DOWNLINK
double MtempoTotalWlanUl = 1;    // Media do tempo total no meio wlan das aplicacoes - DOWNLINK
double MtempoTotalWlanDl = 1;    // Media do tempo total no meio wlan das aplicacoes - DOWNLINK
double MpacoteTotalWlanUl = 1;    // Media do total de pacotes no meio wlan das aplicacoes (size) - UPLINK
double MpacoteTotalWlanDl = 1;    // Media do total de pacotes no meio wlan das aplicacoes (size) - DOWNLINK
std::vector < APP > aplicacoesLteUl;    // Aplicacoes que estao sendo enviada na interface LTe - UPLINK
std::vector < APP > aplicacoesLteDl;    // Aplicacoes que estao sendo enviada na interface LTe - DOWNLINK
std::vector < APP > aplicacoesWlanUl;    // Aplicacoes que estao sendo enviada na interface Wlan - UPLINK
std::vector < APP > aplicacoesWlanDl;    // Aplicacoes que estao sendo enviada na interface Wlan - DOWNLINK
int voipReal=0; // Tentativas de realocacao para aplicacoes VOIP
int videoconfReal=0; // Tentativas de realocacao para aplicacoes VIDEO CONFERENCIA
int videoReal=0; // Tentativa de realocacao para aplciacoes VIDEO
double fatorOcupacaoRo = 0;    // Fator de ocupacao da rede WLAN
double idleTimeWlan = 0;    // Tempo em que o meio esta inativo
double busyTimeWlan = 0;    // Tempo em que o meio esta ocupado
int contador = 0;    // Utilizado para indicar id para as aplicacoes que são criadas na simulacao ( para comparacao do operador sobrescrito '==')

void ConfigurarCenario ( void );
void Decisao ( APP );
void Finalizar ( APP );
void Realocacao ( APP );
void MeioWlan ( );

int main ( int argc, char **argv )
{
   //  To Debug
   // LogComponentEnable ( "TcpL4Protocol", LOG_LEVEL_ALL );
   // LogComponentEnable ( "PacketSink", LOG_LEVEL_ALL );
   // LogComponentEnable ( "OnOffApplication",
//	        LogLevel (
//	                    LOG_LEVEL_ALL | LOG_PREFIX_FUNC
//	                                | LOG_PREFIX_TIME ) );
   // LogComponentEnable ( "UdpSocketImpl", LOG_DEBUG );
   //RTS = request to send && CTS = clear to send

   //  Packet::EnablePrinting ( );
   //  Packet::EnableChecking ( );
   // PacketMetadata::Enable ( );

   numeroDispositivos = atoi ( argv [ 1 ] );
   numeroUsuarios = atof ( argv [ 2 ] );
   quantidadeAplicacoes = atoi ( argv [ 3 ] );

   std::cout << "Leitura dos dados por parametro: "
	        << "\n\tNumero de dispositivos: " << numeroDispositivos
	        << "\n\tNumero de usuarios: " << numeroUsuarios
	        << "\n\tQuantidade de aplicacoes: "
	        << quantidadeAplicacoes << "\n";

   // Configurando o cenario
   ConfigurarCenario ( );
   std::cout << "Configurando cenario.";

   // Calculando o tempo de interchegada
   tempoInterchegada = 3600
	        / ( ( quantidadeAplicacoes
	                    / ( double ) numeroDispositivos ) /* media de chamadas por dispositivos */
	        * numeroUsuarios );
   tempoTotalChamadas = tempoInterchegada * ( - 1 );
   std::cout << "Tempo de interchegada calculado: "
	        << tempoInterchegada << "\n";

   // Gerando aplicacoes
   std::srand ( unsigned ( std::time ( 0 ) ) );
   std::vector < int > aplicacoes;    // somente os tipos das aplicacoes

   // Gerando 33% de Voz
   for ( int i = 0 ; i < quantidadeAplicacoes * 0.33 ; ++i )
      aplicacoes.push_back ( 0 );    // 0 - aplicacao VOIP
   // Gerando 16.5% Video conferencia, 16.5% Video, 
   for ( int i = 0 ; i < quantidadeAplicacoes * 0.165 ; ++i )
   {
      aplicacoes.push_back ( 1 );    // 1 - aplicacao VIDEOCONF
      aplicacoes.push_back ( 2 );    // 2 - aplicacao VIDEO
   }   
   // Gerando 17% WWW e 17% FTP
   for ( int i = 0 ; i < quantidadeAplicacoes * 0.17 ; ++i )
   {
      aplicacoes.push_back ( 3 );    // 3 - aplicacao WWW
      aplicacoes.push_back ( 4 );    // 4 - aplicacao FTP
   }

   // using built-in random generator:
   std::random_shuffle ( aplicacoes.begin ( ), aplicacoes.end ( ) );

   int sender, receiver;
   // Iniciando as aplicacoes
   for ( std::vector < int >::iterator ite = aplicacoes.begin ( ) ;
	        ite != aplicacoes.end ( ) ; ++ite )
   {
      sender = rand ( ) % ( numeroDispositivos );    // 0 ate o numero de ue
      do
      {
	  receiver = rand ( ) % ( numeroDispositivos );
      }
      while ( receiver == sender );

      tempoTotalChamadas += tempoInterchegada;

      APP app;
      switch ( * ite )
      {
	  case 0 :    // VOIP
	     app.id = contador++;
	     app.receiver = receiver;
	     app.sender = sender;
	     app.app = 0;
	     app.status = 0;
	     app.bits = 24 * 1024;
	     app.grauLte = 2;
	     app.grauWlan = 1;
	     app.dataRate = "0.024Mb/s";
	     app.direcao = rand ( ) % ( 2 );    // 0- ul 1- dl
	     Simulator::Schedule ( Seconds ( tempoTotalChamadas ),
		          & Decisao, app );
	     break;
	  case 1 :    // VIDEOCONF
	     app.id = contador++;
	     app.receiver = receiver;
	     app.sender = sender;
	     app.status = 0;
	     app.app = 1;
	     app.bits = 384 * 1024;
	     app.grauLte = 1;
	     app.grauWlan = 2;
	     app.dataRate = "0.384Mb/s";
	     app.direcao = rand ( ) % ( 2 );    // 0- ul 1- dl
	     Simulator::Schedule (
		          Seconds (
		                      Simulator::Now ( ).GetSeconds ( )
		                                  + tempoTotalChamadas ),
		          & Decisao, app );
	     break;
	  case 2 :    // VIDEO
	     app.id = contador++;
	     app.receiver = receiver;
	     app.sender = sender;
	     app.status = 0;
	     app.app = 2;
	     app.grauLte = 1;
	     app.grauWlan = 2;
	     app.dataRate = "0.128Mb/s";
	     app.bits = 131 * 1024;
	     app.direcao = rand ( ) % ( 2 );    // 0- ul 1- dl
	     Simulator::Schedule ( Seconds ( tempoTotalChamadas ),
		          & Decisao, app );
	     break;
	  case 3 :    // WWW
	     app.id = contador++;
	     app.receiver = receiver;
	     app.sender = sender;
	     app.status = 0;
	     app.app = 3;
	     app.grauLte = 1;
	     app.grauWlan = 2;
	     app.dataRate = "0.128Mb/s";
	     app.bits = 131 * 1024;
	     app.direcao = rand ( ) % ( 2 );    // 0- ul 1- dl
	     Simulator::Schedule ( Seconds ( tempoTotalChamadas ),
		          & Decisao, app );
	     break;
	  case 4 :    // FTP
	     app.id = contador++;
	     app.sender = sender;
	     app.receiver = receiver;
	     app.status = 0;
	     app.app = 4;
	     app.grauLte = 1;
	     app.grauWlan = 2;
	     app.dataRate = "0.128Mb/s";
	     app.bits = 131 * 1024;
	     app.direcao = rand ( ) % ( 2 );    // 0- ul 1- dl
	     Simulator::Schedule ( Seconds ( tempoTotalChamadas ),
		          & Decisao, app );
	     break;
      }
   }

   std::cout << "\t----> Número de chamadas criadas: " << contador - 1
	        << "\n";

   Simulator::Schedule ( Seconds ( 0.0 ), & MeioWlan );

   std::cout << "\t--> TEmpo final da simulacao programado para: "
	        << ( quantidadeAplicacoes
	                                / ( ( quantidadeAplicacoes
	                                            / ( double ) numeroDispositivos ) /* media de chamadas por dispositivos */
	                                * numeroUsuarios ) )
	                                * 3600  << "\n";

   Simulator::Stop (
	        Seconds (
	                    ( quantidadeAplicacoes
	                                / ( ( quantidadeAplicacoes
	                                            / ( double ) numeroDispositivos ) /* media de chamadas por dispositivos */
	                                * numeroUsuarios ) )
	                                * 3600 ) );
   Simulator::Run ( );

   std::cout
	        << "-------------------------------------------------------------------------------------\n"
	        << "---> Resultados:" << "\n\tSimulacao executada com:"
	        << "\n\t\tNumero de dispositivos: "
	        << numeroDispositivos << "\n\t\tNumero de usuarios: "
	        << numeroUsuarios << "\n\t\tQuantidade de aplicacoes: "
	        << quantidadeAplicacoes
	        << "\n\tQuantidade de chamadas Lte: " << qtChamadasLte
	        << "\n\tQuantidade de chamadas wlan: "
	        << qtChamadasWlan << "\n\tHandover lte wlan: "
	        << handoverLteWlan << "\n\tHandover wlan lte: "
	        << handoverWlanLte
	        << "\n\tQuantidade aceitas sem renegociacao Real: "
	        << qtAceitasSemRenegociacaoReal
	        << "\n\tQuantidade aceitas com renegociacao Real: "
	        << qtAceitasComRenegociacaoReal
	        << "\n\tQuantidade aceitas sem renegociacao: "
	        << qtAceitasSemRenegociacao
	        << "\n\tQuantidade aceitas com renegociacao: "
	        << qtAceitasComRenegociacao
	        << "\n\tQuantidade aplicacoes:"  
	        << "\n\t\tVoip: " << qtVoipLte <<
	        "\n\t\tVideo conf: " << qtVideoConfLte
	        << "\n\t\tVideo: " << qtVideoLte 
	        << "\n\t\tFtp: " << qtFtpLte
	        << "\n\t\tWww: " << qtWwwLte
	        << "\n\tQuantidade aplicacoes: " << 
	        "\n\t\tVoip: " << qtVoipWlan
	        <<"\n\t\tVideo conf: " << qtVideoConfWlan
	        << "\n\t\tVideo: " << qtVideoWlan
	        << "\n\t\tFtp: " << qtFtpWlan
	        << "\n\t\tWww: " << qtWwwWlan
	        << "\n\tQuantidade de renegociacoes 1: "
	        << qtRenegociacao1
	        << "\n\tQuantidade de renegociacoes 2: "
	        << qtRenegociacao2
	        << "\n\tNumero de realocacoes wlan para lte: "
	        << realocacaoToLte
	        << "\n\tNumero de realocacoes lte para wlan: "
	        << realocacaoToWlan 
	        << "\n\tNumero de tentativas de realocacoes para liberar recursos para: " 
	        << "\n\t\tVOIP: " << voipReal 
	        << "\n\t\tVIDEO CONF: " << videoconfReal 
	        << "\n\t\tVIDEO: " << videoReal << 
	        "\n---------------------\nFim!";

   Simulator::Destroy ( );

   return 0;
}

// Faz a leitura do meio wlan para calcular a media de pacotes
void MeioWlan ( )
{
   std::cout << "Verificando o meio WLAN...";
   MpacoteTotalWlanDl =
	        ( aplicacoesWlanDl.size ( ) != 0
	                    && pacoteTotalWlanDl != 0 ) ?
	                    pacoteTotalWlanDl
	                                / aplicacoesWlanDl.size ( ) :
	                    1;
   MpacoteTotalWlanUl =
	        ( aplicacoesWlanUl.size ( ) != 0
	                    && pacoteTotalWlanUl != 0 ) ?
	                    pacoteTotalWlanUl
	                                / aplicacoesWlanUl.size ( ) :
	                    1;
   std::cout << "\n\tPacote UL: " << MpacoteTotalWlanUl
	        << "\n\tPacote DL: " << MpacoteTotalWlanDl;
   std::cout << "\n\tTempo: ";
   MtempoTotalWlanDl =
	        ( tempoTotalWlanDl != 0 ) ?
	                    tempoTotalWlanDl / ( double ) TEMPO_LEITURA :
	                    1;
   MtempoTotalWlanUl =
	        ( tempoTotalWlanUl != 0 ) ?
	                    tempoTotalWlanUl / ( double ) TEMPO_LEITURA :
	                    1;
   std::cout << "\n\tTempo UL: " << MtempoTotalWlanUl
	        << "\n\\Tempo DL: " << MtempoTotalWlanDl;
   std::cout << "\n\tNova programacao: "
	        << TEMPO_ESPERA + TEMPO_LEITURA << "\n";
   Simulator::Schedule ( Seconds (
   TEMPO_ESPERA + TEMPO_LEITURA ), & MeioWlan );
}

// Realiza a decisao da aplicacao
void Decisao ( APP aplicacao )
{
   std::cout << "Decidindo sobre a aplicacao "
	        << ( ( aplicacao.app == 0 ) ? "VOIP" :
	             ( aplicacao.app == 1 ) ? "VIDEO CONFERENCIA" :
	             ( aplicacao.app == 2 ) ? "VIDEO" :
	             ( aplicacao.app == 3 ) ? "WWW" : "FTP" )
	        << " no tempo " << Simulator::Now ( ).GetSeconds ( )
	        << ".\n";

   // Calculando o fator de ocupacao
   fatorOcupacaoRo =
	        ( busyTimeWlan != 0 || idleTimeWlan != 0 ) ?
	                    ( ( busyTimeWlan
	                                / ( idleTimeWlan + busyTimeWlan ) ) )
	                                + ( ( fatorOcupacaoRo * 0.01 )
	                                            + ( fatorOcupacaoRo
	                                                        * 0.04 )
	                                            + ( fatorOcupacaoRo
	                                                        * 0.1 )
	                                            + ( fatorOcupacaoRo
	                                                        * 0.15 )
	                                            + ( fatorOcupacaoRo
	                                                        * 0.2 ) ) :
	                    0;    // ou recebe zero

   int tendencia;
   if ( aplicacao.direcao )
   {    // Downlink
      std::cout << "\tSentido: DOWNLINK\n";
      tendencia =
	           ( aplicacao.grauLte
	                       * ( ( recursosLteDl + aplicacao.bits )
	                                   <= ( ocupacaoRedeLteDl * 0.8 ) ) )
	                       - ( aplicacao.grauWlan
	                                   * ( ( ( MpacoteTotalWlanDl
	                                               * ( ( ocupacaoRedeWlanDl
	                                                           * 0.80 )
	                                                           - fatorOcupacaoRo ) )
	                                               / MtempoTotalWlanDl )
	                                               >= aplicacao.bits ) );
      std::cout << "\t\tCalculo para tendencia: ("
	           << aplicacao.grauLte << "* (" << recursosLteDl
	           << "<= (" << ocupacaoRedeLteDl << "*" << 0.8
	           << ") ) ) - (" << aplicacao.grauWlan << "* ( ( ("
	           << MpacoteTotalWlanDl << "* ( ("
	           << ocupacaoRedeWlanDl << "* 0.80 ) -"
	           << fatorOcupacaoRo << ") ) /" << MtempoTotalWlanDl
	           << ") >= " << aplicacao.bits << ") )\n";
   }
   else
   {    // Uplink
      std::cout << "\tSentido: UPLINK\n";
      tendencia =
	           ( aplicacao.grauLte
	                       * ( recursosLteUl
	                                   <= ( ocupacaoRedeLteUl * 0.8 ) ) )
	                       - ( aplicacao.grauWlan
	                                   * ( ( ( MpacoteTotalWlanUl
	                                               * ( ( ocupacaoRedeWlanUl
	                                                           * 0.80 )
	                                                           - fatorOcupacaoRo ) )
	                                               / MtempoTotalWlanUl )
	                                               >= aplicacao.bits ) );
      std::cout << "\t\tCalculo para tendencia: ("
	           << aplicacao.grauLte << "* (" << recursosLteUl
	           << "<= (" << ocupacaoRedeLteUl << "*" << 0.8
	           << ") ) ) - (" << aplicacao.grauWlan << "* ( ( ("
	           << MpacoteTotalWlanUl << "* ( ("
	           << ocupacaoRedeWlanUl << "* 0.80 ) -"
	           << fatorOcupacaoRo << ") ) /" << MtempoTotalWlanUl
	           << ") >= " << aplicacao.bits << ") )\n";
   }

   // Caso a tendencia seja 0 - deve ser modificado o staus da aplicacao
   if ( ! tendencia )
   {
      std::cout << "\tTendencia = 0\n";
      switch ( aplicacao.status )
      {
	  case 0 :    // request
	     std::cout
		          << "\t\tModificando status para renegociacao 1.\n";
	     aplicacao.status = 1;
	     aplicacao.bits =
		          ( aplicacao.app == 0 ) ? 12 * 1024 :
		          ( aplicacao.app == 1 ) ?
		                      256 * 1024 : 65 * 1024;

	     // Recolhendo dados (*)
	     qtRenegociacao1++;
	     break;
	  case 1 :    // renegociacao 1
	     std::cout
		          << "\t\tModificando status para renegociacao 2.\n";
	     aplicacao.status = 2;
	     aplicacao.bits =
		          ( aplicacao.app == 0 ) ? 8 * 1024 :
		          ( aplicacao.app == 1 ) ?
		                      124 * 1024 : 32 * 1024;

	     // Recolhendo dados (*)
	     qtRenegociacao2++;
	     break;
	  case 2 :    // renegociacao 2
	     // É somente feito tentativa de realocacao para chamadas VOIP, VIDEOCONF ou VIDEO
	     if ( aplicacao.app <= 2 )
	     {
		 std::cout
			      << "\t\tModificando status para realocacao.\n";
		 aplicacao.status = 3;
		 Realocacao ( aplicacao );
	     }
	     else
	     {
		 std::cout << "\t\tEsta chamada foi negada!\n";
	     }
	     return;
	  case 3 :
	     std::cout
		          << "\t\tEsta chamada passou por tentativa de realocacao, porém foi negada por falta de recursos.\n";
	     return;
      }
      Decisao ( aplicacao );    // volta a decidir sobre esta aplicacao
      return;
   }

   std::cout << "\tTendencia = " << tendencia << "\n";

   if ( tendencia > 0 )
   {    // LTE
      aplicacao.interface = 0;    // define a interface a que a aplicacao sera executada.

      // Recolhendo dados (*)
      qtChamadasLte++;
      if ( aplicacao.app != 0 )
      {
	  handoverWlanLte++;    // caso a chamada aceita no lte seja uma com grau de eligibilidade no wlan, entao foi realizado um handover do wlan para o lte
	  if ( aplicacao.status != 0 )
	     qtAceitasComRenegociacao++;    // esta chamada foi aceita com renegociacao
	  else qtAceitasSemRenegociacao++;    // esta chamada foi aceita sem renegociacao

	  switch (aplicacao.app) {
	  	case 1: // video conferencia
	  		qtVideoConfLte++;
	  		break;
	  	case 2: // video
	  		qtVideoLte++;
	  		break;
	  	case 3: // ww
	  		qtWwwLte++;
	  		break;
	  	case 4: // ftp
	  		qtFtpLte++;
	  		break;
	  }

      }
      else
      {
	  qtVoipLte++;    // incrementa o numero de chamadas voip aceitas no lte
	  if ( aplicacao.status != 0 )
	     qtAceitasComRenegociacaoReal++;    // A chamada VOIP foi aceita com renegociacao
	  else qtAceitasSemRenegociacaoReal++;    // A chamada VOIP foi aceita sem renegociacao
      }

      std::cout << "\tDados recolhidos (lte): \n"
	           << "\t\tQuantidade chamadas lte: " << qtChamadasLte
	           << "\n\t\tHandover Wlan to Lte: " << handoverWlanLte
	           << "\n\t\tQuantidade aceitas com renegociacao: "
	           << qtAceitasComRenegociacao
	           << "\n\t\tQuantidade aceitas sem renegociacao: "
	           << qtAceitasSemRenegociacao
	           << "\n\t\tQuantidade aceitas com renegociacao (real): "
	           << qtAceitasComRenegociacaoReal
	           << "\n\t\tQuantidade aceitas sem renegociacao (real): "
	           << qtAceitasSemRenegociacaoReal
	           << "\n\t\tQuantidade voip: " << qtVoipLte << "\n";

      if ( ! aplicacao.direcao )
      {    // Ul
	  std::cout << "\tAdicionando aplicacao no vetor UL Lte\n";
	  aplicacoesLteUl.push_back ( aplicacao );    // Adicionando esta aplicacao no vetor de aplicacoeses geral
	  recursosLteUl += aplicacao.bits;    // Consumindo dados da interface LTE
	  std::cout << "\tConsumindo do meio UL Lte: " << recursosLteUl
		       << "\n";
      }
      else
      {    // DL
	  std::cout << "\tAdicionando aplicacao no vetor DL Lte\n";
	  aplicacoesLteDl.push_back ( aplicacao );    // Adicionando esta aplicacao no vetor de aplicacoeses geral
	  recursosLteDl += aplicacao.bits;    // Consumindo dados da interface LTE
	  std::cout << "\tConsumindo do meio DL Lte: " << recursosLteDl
		       << "\n";
      }
   }
   else
   {    // WLAN
      aplicacao.interface = 1;    // define a interface a que a aplicacao sera executada.

      // Recolhendo dados (*)
      qtChamadasWlan++;
      if ( ! aplicacao.app )
      {
			qtVoipWlan++;
			handoverLteWlan++;    // caso a chamada aceita no wlan seja uma com o grau de eligibilidade no lte, entao foi realizado um handover do lte para o wlan
			if ( aplicacao.status != 0 )
				qtAceitasComRenegociacaoReal++;    // A chamada VOIP foi aceita com renegociacao
			else qtAceitasSemRenegociacaoReal++;    // A chamada VOIP foi aceita sem renegociacao
			}
      else
      {
			switch ( aplicacao.app )
			{    // incrementa a quantidade de chamadas aceitas no wlan, de acordo com o tipo
			 case 1 :    // VIDEO CONF
			 qtVideoConfWlan++;
			 break;
			 case 2 :    // VIDEO
			 qtVideoWlan++;
			 break;
			 case 3 :    // WWW
			 qtWwwWlan++;
			 break;
			 case 4 :    // FTP
			 qtFtpWlan++;
			 break;
			}
			if ( aplicacao.status != 0 )
			 qtAceitasComRenegociacao++;    // esta chamada foi aceita com renegociacao
			else qtAceitasSemRenegociacao++;    // esta chamada foi aceita sem renegociacao
      }

      std::cout << "\tDados recolhidos (wlan): \n"
	           << "\t\tQuantidade chamadas wlan: "
	           << qtChamadasWlan << "\n\t\tHandover lte to wlan: "
	           << handoverLteWlan
	           << "\n\t\tQuantidade aceitas com renegociacao: "
	           << qtAceitasComRenegociacao
	           << "\n\t\tQuantidade aceitas sem renegociacao: "
	           << qtAceitasSemRenegociacao
	           << "\n\t\tQuantidade aceitas com renegociacao (real): "
	           << qtAceitasComRenegociacaoReal
	           << "\n\t\tQuantidade aceitas sem renegociacao (real): "
	           << qtAceitasSemRenegociacaoReal
	           << "\n\t\tQuantidade video conferencia: "
	           << qtVideoConfWlan << "\n\t\tQuantidade video: "
	           << qtVideoWlan << "\n\t\tQuantidade www: "
	           << qtWwwWlan << "\n\t\tQuantidade ftp: "
	           << qtFtpWlan << "\n";

      if ( ! aplicacao.direcao )
      {    // UL
	  std::cout << "\tAdicionando aplicacao no vetor UL Wlan\n";
	  aplicacoesWlanUl.push_back ( aplicacao );    // Adicionando esta aplicacao no vetor de aplicacoes geral
	  tempoTotalWlanUl += ( aplicacao.app == 3 ) ? 90 : 120;    // Consumindo dados da interface WLAN - tempo
	  pacoteTotalWlanUl += ( aplicacao.app == 0 ) ? 50 : 429;    // Consumindo dados da interface WLAN - pacote
	  std::cout << "\tConsumindo do meio UL Wlan: "
		       << "\n\t\tTempo: " << tempoTotalWlanUl
		       << "\n\t\tPacote: " << pacoteTotalWlanUl << "\n";
      }
      else
      {    // DL
	  std::cout << "\tAdicionando aplicacao no vetor DL Wlan\n";
	  aplicacoesWlanDl.push_back ( aplicacao );    // Adicionando esta aplicacao no vetor de aplicações geral
	  tempoTotalWlanDl += ( aplicacao.app == 3 ) ? 90 : 120;    // Consumindo dados da interface WLAN - tempo
	  pacoteTotalWlanDl += ( aplicacao.app == 0 ) ? 50 : 429;    // Consumindo dados da interface WLAN - pacote
	  std::cout << "\tConsumindo do meio DL Wlan: "
		       << "\n\t\tTempo: " << tempoTotalWlanDl
		       << "\n\t\tPacote: " << pacoteTotalWlanDl << "\n";
      }
   }

   // Criando container de aplicaccoes
   std::cout << "\t Criando container de aplicacoes: \n";
   ApplicationContainer appContainerPacketSink;
   ApplicationContainer appContainerOnOff;
   ns3::Ptr < ns3::Node > sender = ueNode.Get ( aplicacao.sender );
   ns3::Ipv4Address receiverIpAddress = ueNode.Get (
	        aplicacao.receiver )->GetObject < ns3::Ipv4 > ( )
	        ->GetAddress ( ( aplicacao.interface == 0 ) ? 2 : 1,
	        0 ).GetLocal ( );    // Ipv4 Address of the receiver
   std::cout << "\t\tEndereco Ip receiver: " << receiverIpAddress
	        << std::endl;

   // Voz, video conferencia e video são enviados via UDP os demais por TCP
   // Portas: voip = 5060, videoconf = 3230, video = 3235, www = 8080 e ftp = 2121
   ns3::PacketSinkHelper sinkHelper (
	        ( aplicacao.app >= 0 && aplicacao.app <= 2 ) ?
	                    "ns3::UdpSocketFactory" :
	                    "ns3::TcpSocketFactory",
	        ns3::InetSocketAddress ( ns3::Ipv4Address::GetAny ( ),
	                    ( aplicacao.app == 0 ) ? 5060 :
	                    ( aplicacao.app == 1 ) ? 3230 :
	                    ( aplicacao.app == 2 ) ? 3235 :
	                    ( aplicacao.app == 3 ) ? 8080 : 2121 ) );
   appContainerPacketSink = sinkHelper.Install (
	        ueNode.Get ( aplicacao.receiver ) );    // who will receive the packet

   // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
   ns3::OnOffHelper onoff (
	        ( aplicacao.app >= 0 && aplicacao.app <= 2 ) ?
	                    "ns3::UdpSocketFactory" :
	                    "ns3::TcpSocketFactory",
	        ns3::Address ( ) );    // Address() - creates an invalid address.
   onoff.SetAttribute ( "OnTime",
	        ns3::StringValue (
	                    ( aplicacao.app == 0 ) ?
	                                "ns3::ConstantRandomVariable[Constant=120]" :
	                    ( aplicacao.app == 1 ) ?
	                                "ns3::ConstantRandomVariable[Constant=120]" :
	                    ( aplicacao.app == 2 ) ?
	                                "ns3::ConstantRandomVariable[Constant=120]" :
	                    ( aplicacao.app == 3 ) ?
	                                "ns3::ConstantRandomVariable[Constant=120]" :
	                                "ns3::ConstantRandomVariable[Constant=90]" ) );    // On time
   onoff.SetAttribute ( "OffTime",
	        ns3::StringValue (
	                    "ns3::ConstantRandomVariable[Constant=0]" ) );    // Off time
// P.S.: offTime + DataRate/PacketSize = next packet time
   onoff.SetAttribute ( "DataRate",
	        ns3::DataRateValue (
	                    ns3::DataRate ( aplicacao.dataRate ) ) );    // Data Rate
   onoff.SetAttribute ( "PacketSize",
	        ns3::UintegerValue (
	                    ( aplicacao.app == 0 ) ? 50 : 429 ) );    // Packet Size

   ns3::AddressValue receiverAddress (
	        ns3::InetSocketAddress ( receiverIpAddress,
	                    ( aplicacao.app == 0 ) ? 5060 :
	                    ( aplicacao.app == 1 ) ? 3230 :
	                    ( aplicacao.app == 2 ) ? 3235 :
	                    ( aplicacao.app == 3 ) ? 8080 : 2121 ) );
   onoff.SetAttribute ( "Remote", receiverAddress );

   appContainerOnOff = onoff.Install ( sender );

   appContainerOnOff.Start ( Simulator::Now ( ) );
   appContainerOnOff.Stop ( Seconds ( aplicacao.timeTotal ) );
   appContainerPacketSink.Start ( Simulator::Now ( ) );
   appContainerPacketSink.Stop ( Seconds ( aplicacao.timeTotal ) );

   aplicacao.onOff = appContainerOnOff;
   aplicacao.sink = appContainerPacketSink;

   std::cout << "\n\t\tContainer de aplicacoes criado.";

   // Programando a finalizacao desta aplicacao
   Simulator::Schedule (
	        Seconds (
	                    Simulator::Now ( ).GetSeconds ( )
	                                + ( aplicacao.app == 4 ) ?
	                                90.05 : 120.05 ), & Finalizar,
	        aplicacao );
   std::cout << "\n\t\tProgramado para finalizar!";
}

// Finaliza as aplicacoes, retornando os valores consumidos.
void Finalizar ( APP aplicacao )
{
   std::cout << "Finalizando a aplicacao "
	        << ( ( aplicacao.app == 0 ) ? "VOIP" :
	             ( aplicacao.app == 1 ) ? "VIDEO CONFERENCIA" :
	             ( aplicacao.app == 2 ) ? "VIDEO" :
	             ( aplicacao.app == 3 ) ? "WWW" : "FTP" )
	        << " no tempo " << Simulator::Now ( ).GetSeconds ( )
	        << ".\n";

   if ( ! aplicacao.interface )
   {    // LTE
      if ( aplicacao.direcao )
      {    // DL
	  std::cout << "\tRemovendo aplicacao do vetor DL Lte\n";
	  aplicacoesLteDl.erase (
		       std::find ( aplicacoesLteDl.begin ( ),
		                   aplicacoesLteDl.end ( ),
		                   aplicacao ) );
	  recursosLteDl -= aplicacao.bits;
	  std::cout << "\n\tRetornando valores do meio: "
		       << recursosLteDl << "\n";
      }
      else
      {    // UL
	  std::cout << "\tRemovendo aplicacao do vetor UL Lte\n";
	  aplicacoesLteUl.erase (
		       std::find ( aplicacoesLteUl.begin ( ),
		                   aplicacoesLteUl.end ( ),
		                   aplicacao ) );
	  recursosLteUl -= aplicacao.bits;
	  std::cout << "\n\tRetornando valores do meio: "
		       << recursosLteUl << "\n";
      }
   }
   else
   {    // WLAN
      if ( aplicacao.direcao )
      {    // DL
	  std::cout << "\tRemovendo aplicacao do vetor DL Wlan\n";
	  aplicacoesWlanDl.erase (
		       std::find ( aplicacoesWlanDl.begin ( ),
		                   aplicacoesWlanDl.end ( ),
		                   aplicacao ) );

	  tempoTotalWlanDl -= ( aplicacao.app == 3 ) ? 90 : 120;    // Retornando dados da interface WLAN - tempo
	  pacoteTotalWlanDl -= ( aplicacao.app == 0 ) ? 50 : 429;    // Retornando dados da interface WLAN - pacote
	  std::cout << "\n\tRetornando valores do meio: "
		       << "\n\t\tTempo: " << tempoTotalWlanDl
		       << "\n\t\tPacote: " << pacoteTotalWlanDl << "\n";
      }
      else
      {    // UL
	  std::cout << "\tRemovendo aplicacao do vetor UL Wlan\n";
	  aplicacoesWlanUl.erase (
		       std::find ( aplicacoesWlanUl.begin ( ),
		                   aplicacoesWlanUl.end ( ),
		                   aplicacao ) );
	  tempoTotalWlanUl -= ( aplicacao.app == 3 ) ? 90 : 120;    // Retornando dados da interface WLAN - tempo
	  pacoteTotalWlanUl -= ( aplicacao.app == 0 ) ? 50 : 429;    // Retornando dados da interface WLAN - pacote
	  std::cout << "\n\tRetornando valores do meio: "
		       << "\n\t\tTempo: " << tempoTotalWlanUl
		       << "\n\t\tPacote: " << pacoteTotalWlanUl << "\n";
      }
   }
}

// Realoca as chamadas que estao sendo executadas para que a aplicacao em questao seja aceita.
void Realocacao ( APP aplicacao )
{
   std::cout << "Realocando a aplicacao "
	        << ( ( aplicacao.app == 0 ) ? "VOIP" :
	             ( aplicacao.app == 1 ) ? "VIDEO CONFERENCIA" :
	             ( aplicacao.app == 2 ) ? "VIDEO" :
	             ( aplicacao.app == 3 ) ? "WWW" : "FTP" )
	        << " no tempo " << Simulator::Now ( ).GetSeconds ( )
	        << ".\n";

   switch ( aplicacao.app )
   {
      case 0 :    // VOIP
	  // Liberando recursos na interface LTE para que esta aplicacao seja enviada.
	  voipReal++; // mais uma aplicacao voip, tentando realocacoes para liberacao de recursos
	  if ( aplicacao.direcao )
	  {    // DL
	     std::vector < APP >::iterator ite =
		          aplicacoesLteDl.begin ( );
	     while ( ite != aplicacoesLteDl.end ( )
		          && ( recursosLteDl + aplicacao.bits )
		                      > ( ocupacaoRedeLteDl * 0.8 ) )
	     {    // Enquanto nao houver recursos ou finalizar as as aplicacoes
		 if ( ( * ite ).app != 0
			      && ( ( ( MpacoteTotalWlanDl
			                  * ( ( ocupacaoRedeWlanDl * 0.80 )
			                              - fatorOcupacaoRo ) )
			                  / MtempoTotalWlanDl )
			                  >= ( * ite ).bits ) )
		 {    // realocar chamada do Lte para o Wlan
		    ( * ite ).interface = 1;    // modificando interface

		    ( * ite ).onOff.Stop ( Simulator::Now ( ) );    // finalizando aplicacao criada anteriormente
		    ( * ite ).sink.Stop ( Simulator::Now ( ) );    // finalizando aplicacao criada anteriormente

		    // Criando novo container de aplicaccoes
		    std::cout
			         << "\tCriando container de aplicacoes: \n";
		    ApplicationContainer appContainerPacketSink;
		    ApplicationContainer appContainerOnOff;
		    ns3::Ptr < ns3::Node > sender = ueNode.Get (
			         ( * ite ).sender );
		    ns3::Ipv4Address receiverIpAddress = ueNode.Get (
			         ( * ite ).receiver )
			         ->GetObject < ns3::Ipv4 > ( )
			         ->GetAddress (
			         ( ( * ite ).interface == 0 ) ? 2 : 1,
			         0 ).GetLocal ( );    // Ipv4 Address of the receiver
		    std::cout << "\t\tEndereco Ip receiver: "
			         << receiverIpAddress << std::endl;

		    // Voz, video conferencia e video são enviados via UDP os demais por TCP
		    // Portas: voip = 5060, videoconf = 3230, video = 3235, www = 8080 e ftp = 2121
		    ns3::PacketSinkHelper sinkHelper (
			         ( ( * ite ).app >= 0
			                     && ( * ite ).app <= 2 ) ?
			                     "ns3::UdpSocketFactory" :
			                     "ns3::TcpSocketFactory",
			         ns3::InetSocketAddress (
			                     ns3::Ipv4Address::GetAny ( ),
			                     ( ( * ite ).app == 0 ) ?
			                                 5060 :
			                     ( ( * ite ).app == 1 ) ?
			                                 3230 :
			                     ( ( * ite ).app == 2 ) ?
			                                 3235 :
			                     ( ( * ite ).app == 3 ) ?
			                                 8080 : 2121 ) );
		    appContainerPacketSink = sinkHelper.Install (
			         ueNode.Get ( ( * ite ).receiver ) );    // who will receive the packet

		    // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
		    ns3::OnOffHelper onoff (
			         ( ( * ite ).app >= 0
			                     && ( * ite ).app <= 2 ) ?
			                     "ns3::UdpSocketFactory" :
			                     "ns3::TcpSocketFactory",
			         ns3::Address ( ) );    // Address() - creates an invalid address.
		    onoff.SetAttribute ( "OnTime",
			         ns3::StringValue (
			                     ( ( * ite ).app == 0 ) ?
			                                 "ns3::ConstantRandomVariable[Constant=120]" :
			                     ( ( * ite ).app == 1 ) ?
			                                 "ns3::ConstantRandomVariable[Constant=120]" :
			                     ( ( * ite ).app == 2 ) ?
			                                 "ns3::ConstantRandomVariable[Constant=120]" :
			                     ( ( * ite ).app == 3 ) ?
			                                 "ns3::ExponentialVariable[120]" :
			                                 "ns3::ExponentialVariable[90]" ) );    // On time
		    onoff.SetAttribute ( "OffTime",
			         ns3::StringValue (
			                     "ns3::ConstantRandomVariable[Constant=0]" ) );    // Off time
		    // P.S.: offTime + DataRate/PacketSize = next packet time
		    onoff.SetAttribute ( "DataRate",
			         ns3::DataRateValue (
			                     ns3::DataRate (
			                                 ( * ite ).dataRate ) ) );    // Data Rate
		    onoff.SetAttribute ( "PacketSize",
			         ns3::UintegerValue (
			                     ( ( * ite ).app == 0 ) ?
			                                 50 : 429 ) );    // Packet Size

		    ns3::AddressValue receiverAddress (
			         ns3::InetSocketAddress (
			                     receiverIpAddress,
			                     ( ( * ite ).app == 0 ) ?
			                                 5060 :
			                     ( ( * ite ).app == 1 ) ?
			                                 3230 :
			                     ( ( * ite ).app == 2 ) ?
			                                 3235 :
			                     ( ( * ite ).app == 3 ) ?
			                                 8080 : 2121 ) );
		    onoff.SetAttribute ( "Remote", receiverAddress );

		    appContainerOnOff = onoff.Install ( sender );

		    appContainerOnOff.Start ( Simulator::Now ( ) );
		    appContainerOnOff.Stop (
			         Seconds ( ( * ite ).timeTotal ) );
		    appContainerPacketSink.Start ( Simulator::Now ( ) );
		    appContainerPacketSink.Stop (
			         Seconds ( ( * ite ).timeTotal ) );

		    ( * ite ).onOff = appContainerOnOff;
		    ( * ite ).sink = appContainerPacketSink;

		    // Recolhendo dados (*)
		    realocacaoToWlan++;    // esta chamada foi realocada do lte para o wlan

		    std::cout << "\tRealocacao para o wlan: "
			         << realocacaoToWlan << "\n";

		    // Liberando recursos no Lte, pois a chamada foi realocada para o wlan
		    std::cout
			         << "\tRemovendo aplicacao do vetor DL Lte\n";
		    aplicacoesLteDl.erase (
			         std::find ( aplicacoesLteDl.begin ( ),
			                     aplicacoesLteDl.end ( ),
			                     ( * ite ) ) );
		    recursosLteDl -= ( * ite ).bits;
		    std::cout << "\n\tRetornando valores do meio: "
			         << recursosLteDl << "\n";

		    // Consumindo recursos do Wlan
		    std::cout
			         << "\tAdicionando aplicacao no vetor DL Wlan\n";
		    aplicacoesWlanDl.push_back ( ( * ite ) );    // Adicionando esta aplicacao no vetor de aplicações geral
		    tempoTotalWlanDl +=
			         ( ( * ite ).app == 3 ) ? 90 : 120;    // Consumindo dados da interface WLAN - tempo
		    pacoteTotalWlanDl +=
			         ( ( * ite ).app == 0 ) ? 50 : 429;    // Consumindo dados da interface WLAN - pacote
		    std::cout << "\tConsumindo do meio DL Wlan: "
			         << "\n\t\tTempo: " << tempoTotalWlanDl
			         << "\n\t\tPacote: " << pacoteTotalWlanDl
			         << "\n";

		 }
		 ++ite;    // avanca a aplicacao
	     }
	  }
	  else
	  {    // UL
	     std::vector < APP >::iterator ite =
		          aplicacoesLteUl.begin ( );
	     while ( ite != aplicacoesLteUl.end ( )
		          && ( recursosLteUl + aplicacao.bits )
		                      > ( ocupacaoRedeLteUl * 0.8 ) )
	     {    // Enquanto nao houver recursos ou finalizar as as aplicacoes
		 if ( ( * ite ).app != 0
			      && ( ( ( MpacoteTotalWlanUl
			                  * ( ( ocupacaoRedeWlanUl * 0.80 )
			                              - fatorOcupacaoRo ) )
			                  / MtempoTotalWlanUl )
			                  >= ( * ite ).bits ) )
		 {    // realocar chamada do Lte para o Wlan
		    ( * ite ).interface = 1;    // modificando interface

		    ( * ite ).onOff.Stop ( Simulator::Now ( ) );    // finalizando aplicacao criada anteriormente
		    ( * ite ).sink.Stop ( Simulator::Now ( ) );    // finalizando aplicacao criada anteriormente

		    // Criando novo container de aplicaccoes
		    std::cout
			         << "\tCriando container de aplicacoes: \n";
		    ApplicationContainer appContainerPacketSink;
		    ApplicationContainer appContainerOnOff;
		    ns3::Ptr < ns3::Node > sender = ueNode.Get (
			         ( * ite ).sender );
		    ns3::Ipv4Address receiverIpAddress = ueNode.Get (
			         ( * ite ).receiver )
			         ->GetObject < ns3::Ipv4 > ( )
			         ->GetAddress (
			         ( ( * ite ).interface == 0 ) ? 2 : 1,
			         0 ).GetLocal ( );    // Ipv4 Address of the receiver
		    std::cout << "\t\tEndereco Ip receiver: "
			         << receiverIpAddress << std::endl;

		    // Voz, video conferencia e video são enviados via UDP os demais por TCP
		    // Portas: voip = 5060, videoconf = 3230, video = 3235, www = 8080 e ftp = 2121
		    ns3::PacketSinkHelper sinkHelper (
			         ( ( * ite ).app >= 0
			                     && ( * ite ).app <= 2 ) ?
			                     "ns3::UdpSocketFactory" :
			                     "ns3::TcpSocketFactory",
			         ns3::InetSocketAddress (
			                     ns3::Ipv4Address::GetAny ( ),
			                     ( ( * ite ).app == 0 ) ?
			                                 5060 :
			                     ( ( * ite ).app == 1 ) ?
			                                 3230 :
			                     ( ( * ite ).app == 2 ) ?
			                                 3235 :
			                     ( ( * ite ).app == 3 ) ?
			                                 8080 : 2121 ) );
		    appContainerPacketSink = sinkHelper.Install (
			         ueNode.Get ( ( * ite ).receiver ) );    // who will receive the packet

		    // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
		    ns3::OnOffHelper onoff (
			         ( ( * ite ).app >= 0
			                     && ( * ite ).app <= 2 ) ?
			                     "ns3::UdpSocketFactory" :
			                     "ns3::TcpSocketFactory",
			         ns3::Address ( ) );    // Address() - creates an invalid address.
		    onoff.SetAttribute ( "OnTime",
			         ns3::StringValue (
			                     ( ( * ite ).app == 0 ) ?
			                                 "ns3::ConstantRandomVariable[Constant=120]" :
			                     ( ( * ite ).app == 1 ) ?
			                                 "ns3::ConstantRandomVariable[Constant=120]" :
			                     ( ( * ite ).app == 2 ) ?
			                                 "ns3::ConstantRandomVariable[Constant=120]" :
			                     ( ( * ite ).app == 3 ) ?
			                                 "ns3::ExponentialVariable[120]" :
			                                 "ns3::ExponentialVariable[90]" ) );    // On time
		    onoff.SetAttribute ( "OffTime",
			         ns3::StringValue (
			                     "ns3::ConstantRandomVariable[Constant=0]" ) );    // Off time
		    // P.S.: offTime + DataRate/PacketSize = next packet time
		    onoff.SetAttribute ( "DataRate",
			         ns3::DataRateValue (
			                     ns3::DataRate (
			                                 ( * ite ).dataRate ) ) );    // Data Rate
		    onoff.SetAttribute ( "PacketSize",
			         ns3::UintegerValue (
			                     ( ( * ite ).app == 0 ) ?
			                                 50 : 429 ) );    // Packet Size

		    ns3::AddressValue receiverAddress (
			         ns3::InetSocketAddress (
			                     receiverIpAddress,
			                     ( ( * ite ).app == 0 ) ?
			                                 5060 :
			                     ( ( * ite ).app == 1 ) ?
			                                 3230 :
			                     ( ( * ite ).app == 2 ) ?
			                                 3235 :
			                     ( ( * ite ).app == 3 ) ?
			                                 8080 : 2121 ) );
		    onoff.SetAttribute ( "Remote", receiverAddress );

		    appContainerOnOff = onoff.Install ( sender );

		    appContainerOnOff.Start ( Simulator::Now ( ) );
		    appContainerOnOff.Stop (
			         Seconds ( ( * ite ).timeTotal ) );
		    appContainerPacketSink.Start ( Simulator::Now ( ) );
		    appContainerPacketSink.Stop (
			         Seconds ( ( * ite ).timeTotal ) );

		    ( * ite ).onOff = appContainerOnOff;
		    ( * ite ).sink = appContainerPacketSink;

		    // Recolhendo dados (*)
		    realocacaoToWlan++;    // esta chamada foi realocada do lte para o wlan

		    std::cout << "\tRealocacao para o wlan: "
			         << realocacaoToWlan << "\n";

		    // Liberando recursos no Lte, pois a chamada foi realocada para o wlan
		    std::cout
			         << "\tRemovendo aplicacao do vetor UL Lte\n";
		    aplicacoesLteUl.erase (
			         std::find ( aplicacoesLteUl.begin ( ),
			                     aplicacoesLteUl.end ( ),
			                     ( * ite ) ) );
		    recursosLteUl -= ( * ite ).bits;
		    std::cout << "\n\tRetornando valores do meio: "
			         << recursosLteUl << "\n";

		    // Consumindo recursos do Wlan
		    std::cout
			         << "\tAdicionando aplicacao no vetor UL Wlan\n";
		    aplicacoesWlanUl.push_back ( ( * ite ) );    // Adicionando esta aplicacao no vetor de aplicações geral
		    tempoTotalWlanUl +=
			         ( ( * ite ).app == 3 ) ? 90 : 120;    // Consumindo dados da interface WLAN - tempo
		    pacoteTotalWlanUl +=
			         ( ( * ite ).app == 0 ) ? 50 : 429;    // Consumindo dados da interface WLAN - pacote
		    std::cout << "\tConsumindo do meio UL Wlan: "
			         << "\n\t\tTempo: " << tempoTotalWlanUl
			         << "\n\t\tPacote: " << pacoteTotalWlanUl
			         << "\n";

		 }
		 ++ite;    // avanca a aplicacao
	     }
	  }
	  break;
      case 1 :    // VIDEO CONF            
      case 2 :    // VIDEO
	           // Liberando recursos na interface WLAN para que esta aplicacao seja enviada.
      if (aplicacao.app == 1) // VIDEO CONF
      	videoconfReal++; // mais uma aplicacao video conferencia, tentando realocar para liberar recursos
      else
      	videoReal++; // mais uma aplicacao video, tentando realocar para liberar recursos

	  if ( aplicacao.direcao )
	  {    // DL
	     std::vector < APP >::iterator ite =
		          aplicacoesWlanDl.begin ( );
	     while ( ite != aplicacoesWlanDl.end ( )
		          && ( ( ( MpacoteTotalWlanDl
		                      * ( ( ocupacaoRedeWlanDl * 0.80 )
		                                  - fatorOcupacaoRo ) )
		                      / MtempoTotalWlanDl )
		                      < aplicacao.bits ) )
	     {    // Enquanto nao houver recursos ou finalizar as as aplicacoes
		 if ( ( recursosLteDl + ( * ite ).bits )
			      <= ( ocupacaoRedeLteDl * 0.8 ) )
		 {    // se houver recursos no lte
		    ( * ite ).interface = 0;    // modificando interface

		    ( * ite ).onOff.Stop ( Simulator::Now ( ) );    // finalizando aplicacao criada anteriormente
		    ( * ite ).sink.Stop ( Simulator::Now ( ) );    // finalizando aplicacao criada anteriormente

		    // Criando novo container de aplicaccoes
		    std::cout
			         << "\tCriando container de aplicacoes: \n";
		    ApplicationContainer appContainerPacketSink;
		    ApplicationContainer appContainerOnOff;
		    ns3::Ptr < ns3::Node > sender = ueNode.Get (
			         ( * ite ).sender );
		    ns3::Ipv4Address receiverIpAddress = ueNode.Get (
			         ( * ite ).receiver )
			         ->GetObject < ns3::Ipv4 > ( )
			         ->GetAddress (
			         ( ( * ite ).interface == 0 ) ? 2 : 1,
			         0 ).GetLocal ( );    // Ipv4 Address of the receiver
		    std::cout << "\t\tEndereco Ip receiver: "
			         << receiverIpAddress << std::endl;

		    // Voz, video conferencia e video são enviados via UDP os demais por TCP
		    // Portas: voip = 5060, videoconf = 3230, video = 3235, www = 8080 e ftp = 2121
		    ns3::PacketSinkHelper sinkHelper (
			         ( ( * ite ).app >= 0
			                     && ( * ite ).app <= 2 ) ?
			                     "ns3::UdpSocketFactory" :
			                     "ns3::TcpSocketFactory",
			         ns3::InetSocketAddress (
			                     ns3::Ipv4Address::GetAny ( ),
			                     ( ( * ite ).app == 0 ) ?
			                                 5060 :
			                     ( ( * ite ).app == 1 ) ?
			                                 3230 :
			                     ( ( * ite ).app == 2 ) ?
			                                 3235 :
			                     ( ( * ite ).app == 3 ) ?
			                                 8080 : 2121 ) );
		    appContainerPacketSink = sinkHelper.Install (
			         ueNode.Get ( ( * ite ).receiver ) );    // who will receive the packet

		    // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
		    ns3::OnOffHelper onoff (
			         ( ( * ite ).app >= 0
			                     && ( * ite ).app <= 2 ) ?
			                     "ns3::UdpSocketFactory" :
			                     "ns3::TcpSocketFactory",
			         ns3::Address ( ) );    // Address() - creates an invalid address.
		    onoff.SetAttribute ( "OnTime",
			         ns3::StringValue (
			                     ( ( * ite ).app == 0 ) ?
			                                 "ns3::ConstantRandomVariable[Constant=120]" :
			                     ( ( * ite ).app == 1 ) ?
			                                 "ns3::ConstantRandomVariable[Constant=120]" :
			                     ( ( * ite ).app == 2 ) ?
			                                 "ns3::ConstantRandomVariable[Constant=120]" :
			                     ( ( * ite ).app == 3 ) ?
			                                 "ns3::ExponentialVariable[120]" :
			                                 "ns3::ExponentialVariable[90]" ) );    // On time
		    onoff.SetAttribute ( "OffTime",
			         ns3::StringValue (
			                     "ns3::ConstantRandomVariable[Constant=0]" ) );    // Off time
		    // P.S.: offTime + DataRate/PacketSize = next packet time
		    onoff.SetAttribute ( "DataRate",
			         ns3::DataRateValue (
			                     ns3::DataRate (
			                                 ( * ite ).dataRate ) ) );    // Data Rate
		    onoff.SetAttribute ( "PacketSize",
			         ns3::UintegerValue (
			                     ( ( * ite ).app == 0 ) ?
			                                 50 : 429 ) );    // Packet Size

		    ns3::AddressValue receiverAddress (
			         ns3::InetSocketAddress (
			                     receiverIpAddress,
			                     ( ( * ite ).app == 0 ) ?
			                                 5060 :
			                     ( ( * ite ).app == 1 ) ?
			                                 3230 :
			                     ( ( * ite ).app == 2 ) ?
			                                 3235 :
			                     ( ( * ite ).app == 3 ) ?
			                                 8080 : 2121 ) );
		    onoff.SetAttribute ( "Remote", receiverAddress );

		    appContainerOnOff = onoff.Install ( sender );

		    appContainerOnOff.Start ( Simulator::Now ( ) );
		    appContainerOnOff.Stop (
			         Seconds ( ( * ite ).timeTotal ) );
		    appContainerPacketSink.Start ( Simulator::Now ( ) );
		    appContainerPacketSink.Stop (
			         Seconds ( ( * ite ).timeTotal ) );

		    ( * ite ).onOff = appContainerOnOff;
		    ( * ite ).sink = appContainerPacketSink;

		    // Recolhendo dados (*)
		    realocacaoToLte++;    // esta chamada foi realocada do wlan para o lte

		    std::cout << "\tRealocacao para o lte: "
			         << realocacaoToLte << "\n";

		    // Liberando recursos no wlan, pois a chamada foi realocada para o lte
		    std::cout
			         << "\tRemovendo aplicacao do vetor DL WLAN\n";
		    aplicacoesWlanDl.erase (
			         std::find ( aplicacoesWlanDl.begin ( ),
			                     aplicacoesWlanDl.end ( ),
			                     ( * ite ) ) );
		    tempoTotalWlanDl -=
			         ( ( * ite ).app == 3 ) ? 90 : 120;    // Retornando dados da interface WLAN - tempo
		    pacoteTotalWlanDl -=
			         ( ( * ite ).app == 0 ) ? 50 : 429;    // Retornando dados da interface WLAN - pacote
		    std::cout
			         << "\tRetornando valores do meio DL WLAN: "
			         << "\n\t\tTempo: " << tempoTotalWlanDl
			         << "\n\t\tPacote: " << pacoteTotalWlanDl
			         << "\n";

		    // Consumindo recursos do Lte
		    std::cout
			         << "\tAdicionando aplicacao no vetor DL Lte\n";
		    aplicacoesLteDl.push_back ( ( * ite ) );
		    recursosLteDl += ( * ite ).bits;
		    std::cout << "\n\tConsumindo valores do meio: "
			         << recursosLteDl << "\n";
		 }
		 ++ite;    // avanca a aplicacao
	     }
	  }
	  else
	  {    // UL
	     std::vector < APP >::iterator ite =
		          aplicacoesWlanUl.begin ( );
	     while ( ite != aplicacoesWlanUl.end ( )
		          && ( ( ( MpacoteTotalWlanUl
		                      * ( ( ocupacaoRedeWlanUl * 0.80 )
		                                  - fatorOcupacaoRo ) )
		                      / MtempoTotalWlanUl )
		                      >= aplicacao.bits ) )
	     {    // Enquanto nao houver recursos ou finalizar as as aplicacoes
		 if ( ( recursosLteUl + ( * ite ).bits )
			      <= ( ocupacaoRedeLteUl * 0.8 ) )
		 {    // se houver recursos no lte
		    ( * ite ).interface = 0;    // modificando interface

		    ( * ite ).onOff.Stop ( Simulator::Now ( ) );    // finalizando aplicacao criada anteriormente
		    ( * ite ).sink.Stop ( Simulator::Now ( ) );    // finalizando aplicacao criada anteriormente

		    // Criando novo container de aplicaccoes
		    std::cout
			         << "\tCriando container de aplicacoes: \n";
		    ApplicationContainer appContainerPacketSink;
		    ApplicationContainer appContainerOnOff;
		    ns3::Ptr < ns3::Node > sender = ueNode.Get (
			         ( * ite ).sender );
		    ns3::Ipv4Address receiverIpAddress = ueNode.Get (
			         ( * ite ).receiver )
			         ->GetObject < ns3::Ipv4 > ( )
			         ->GetAddress (
			         ( ( * ite ).interface == 0 ) ? 2 : 1,
			         0 ).GetLocal ( );    // Ipv4 Address of the receiver
		    std::cout << "\t\tEndereco Ip receiver: "
			         << receiverIpAddress << std::endl;

		    // Voz, video conferencia e video são enviados via UDP os demais por TCP
		    // Portas: voip = 5060, videoconf = 3230, video = 3235, www = 8080 e ftp = 2121
		    ns3::PacketSinkHelper sinkHelper (
			         ( ( * ite ).app >= 0
			                     && ( * ite ).app <= 2 ) ?
			                     "ns3::UdpSocketFactory" :
			                     "ns3::TcpSocketFactory",
			         ns3::InetSocketAddress (
			                     ns3::Ipv4Address::GetAny ( ),
			                     ( ( * ite ).app == 0 ) ?
			                                 5060 :
			                     ( ( * ite ).app == 1 ) ?
			                                 3230 :
			                     ( ( * ite ).app == 2 ) ?
			                                 3235 :
			                     ( ( * ite ).app == 3 ) ?
			                                 8080 : 2121 ) );
		    appContainerPacketSink = sinkHelper.Install (
			         ueNode.Get ( ( * ite ).receiver ) );    // who will receive the packet

		    // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
		    ns3::OnOffHelper onoff (
			         ( ( * ite ).app >= 0
			                     && ( * ite ).app <= 2 ) ?
			                     "ns3::UdpSocketFactory" :
			                     "ns3::TcpSocketFactory",
			         ns3::Address ( ) );    // Address() - creates an invalid address.
		    onoff.SetAttribute ( "OnTime",
			         ns3::StringValue (
			                     ( ( * ite ).app == 0 ) ?
			                                 "ns3::ConstantRandomVariable[Constant=120]" :
			                     ( ( * ite ).app == 1 ) ?
			                                 "ns3::ConstantRandomVariable[Constant=120]" :
			                     ( ( * ite ).app == 2 ) ?
			                                 "ns3::ConstantRandomVariable[Constant=120]" :
			                     ( ( * ite ).app == 3 ) ?
			                                 "ns3::ExponentialVariable[120]" :
			                                 "ns3::ExponentialVariable[90]" ) );    // On time
		    onoff.SetAttribute ( "OffTime",
			         ns3::StringValue (
			                     "ns3::ConstantRandomVariable[Constant=0]" ) );    // Off time
		    // P.S.: offTime + DataRate/PacketSize = next packet time
		    onoff.SetAttribute ( "DataRate",
			         ns3::DataRateValue (
			                     ns3::DataRate (
			                                 ( * ite ).dataRate ) ) );    // Data Rate
		    onoff.SetAttribute ( "PacketSize",
			         ns3::UintegerValue (
			                     ( ( * ite ).app == 0 ) ?
			                                 50 : 429 ) );    // Packet Size

		    ns3::AddressValue receiverAddress (
			         ns3::InetSocketAddress (
			                     receiverIpAddress,
			                     ( ( * ite ).app == 0 ) ?
			                                 5060 :
			                     ( ( * ite ).app == 1 ) ?
			                                 3230 :
			                     ( ( * ite ).app == 2 ) ?
			                                 3235 :
			                     ( ( * ite ).app == 3 ) ?
			                                 8080 : 2121 ) );
		    onoff.SetAttribute ( "Remote", receiverAddress );

		    appContainerOnOff = onoff.Install ( sender );

		    appContainerOnOff.Start ( Simulator::Now ( ) );
		    appContainerOnOff.Stop (
			         Seconds ( ( * ite ).timeTotal ) );
		    appContainerPacketSink.Start ( Simulator::Now ( ) );
		    appContainerPacketSink.Stop (
			         Seconds ( ( * ite ).timeTotal ) );

		    ( * ite ).onOff = appContainerOnOff;
		    ( * ite ).sink = appContainerPacketSink;

		    // Recolhendo dados (*)
		    realocacaoToLte++;    // esta chamada foi realocada do wlan para o lte

		    std::cout << "\tRealocacao para o lte: "
			         << realocacaoToLte << "\n";

		    // Liberando recursos no wlan, pois a chamada foi realocada para o lte
		    std::cout
			         << "\tRemovendo aplicacao do vetor Ul WLAN\n";
		    aplicacoesWlanUl.erase (
			         std::find ( aplicacoesWlanUl.begin ( ),
			                     aplicacoesWlanUl.end ( ),
			                     ( * ite ) ) );
		    tempoTotalWlanUl -=
			         ( ( * ite ).app == 3 ) ? 90 : 120;    // Retornando dados da interface WLAN - tempo
		    pacoteTotalWlanUl -=
			         ( ( * ite ).app == 0 ) ? 50 : 429;    // Retornando dados da interface WLAN - pacote
		    std::cout
			         << "\tRetornando valores do meio Ul WLAN: "
			         << "\n\t\tTempo: " << tempoTotalWlanUl
			         << "\n\t\tPacote: " << pacoteTotalWlanUl
			         << "\n";

		    // Consumindo recursos do Lte
		    std::cout
			         << "\tAdicionando aplicacao no vetor UL Lte\n";
		    aplicacoesLteUl.push_back ( ( * ite ) );
		    recursosLteUl += ( * ite ).bits;
		    std::cout << "\n\tConsumindo valores do meio: "
			         << recursosLteUl << "\n";
		 }
		 ++ite;    // avanca a aplicacao
	     }
	  }
	  break;
   }
   /*
    * 	Após ter executado as tecnicas de realocacao para liberar recursos para esta aplicacao
    * 	 é executado a decisao sobre esta aplicacao para verificar se a realocacao foi suficiente.
    */
   Decisao ( aplicacao );
   return;
}

// Configurando o cenario para a simulacao
void ConfigurarCenario ( void )
{
// Config::SetDefault("ns3::TcpL4Protocol::SocketType", ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("2200"))
   /*
    *   Srs Periodicity
    *          m_nUes < 25 = 40
    *          m_nUes < 60 = 80
    *          m_nUes < 120 = 160
    *          m_nUes >= 120 = 320
    *
    *   http://www.nsnam.org/doxygen-release/test-lte-rrc_8cc_source.html
    */
   Config::SetDefault ( "ns3::LteEnbRrc::SrsPeriodicity",
	        UintegerValue (
	        /*( numeroDispositivos < 25 ) ? 40 :
	         ( numeroDispositivos < 60 ) ? 80 :
	         ( numeroDispositivos < 120 ) ?
	         160 : 320 */320 ) );
   Config::SetDefault (
	        "ns3::WifiRemoteStationManager::RtsCtsThreshold",
	        StringValue ( "2200" ) );
   Config::SetDefault (
	        "ns3::WifiRemoteStationManager::NonUnicastMode",
	        StringValue ( "DsssRate11Mbps" ) );

   lteHelper = CreateObject < LteHelper > ( );    // LTE
   lteHelper->SetSchedulerType ( "ns3::PfFfMacScheduler" );
   lteHelper->SetAttribute ( "PathlossModel",
	        StringValue ( "ns3::Cost231PropagationLossModel" ) );
   epcHelper = CreateObject < EpcHelper > ( );    // EPC
   lteHelper->SetEpcHelper ( epcHelper );
   pgwNode = epcHelper->GetPgwNode ( );    // PGW
   enbNode.Create ( 1 );    // Evolved Node B
   ueNode.Create ( numeroDispositivos );
   haNode.Create ( 1 );    // Home Agent
   apNode = CreateObject < Node > ( );    // Access Point node

   Ptr < ListPositionAllocator > pgwPosition = CreateObject <
	        ListPositionAllocator > ( );
   pgwPosition->Add ( Vector ( 1200, 1200, 0 ) );
   mobilityHelper.SetPositionAllocator ( pgwPosition );
   mobilityHelper.SetMobilityModel (
	        "ns3::ConstantPositionMobilityModel" );
   mobilityHelper.Install ( pgwNode );    // Packet Data Network Gateway

   Ptr < ListPositionAllocator > enblPosition = CreateObject <
	        ListPositionAllocator > ( );
   enblPosition->Add ( Vector ( 1100, 1346, 0 ) );
   mobilityHelper.SetPositionAllocator ( enblPosition );
   mobilityHelper.SetMobilityModel (
	        "ns3::ConstantPositionMobilityModel" );
   mobilityHelper.Install ( enbNode.Get ( 0 ) );    // Enhanced node B

   Ptr < ListPositionAllocator > serverPosition = CreateObject <
	        ListPositionAllocator > ( );
   serverPosition->Add ( Vector ( 1270, 1200, 0 ) );
   mobilityHelper.SetPositionAllocator ( serverPosition );
   mobilityHelper.SetMobilityModel (
	        "ns3::ConstantPositionMobilityModel" );
   mobilityHelper.Install ( haNode );    // HA Node

   mobilityHelper.SetPositionAllocator (
	        "ns3::RandomDiscPositionAllocator", "X",
	        StringValue ( "1150.0" ), "Y",
	        StringValue ( "1346.0" ), "Rho",
	        StringValue (
	                    "ns3::UniformRandomVariable[Min=1|Max=24]" ) );
   mobilityHelper.SetMobilityModel ( "ns3::RandomWalk2dMobilityModel",
	        "Mode", StringValue ( "Time" ), "Time",
	        StringValue ( "2s" ), "Speed",
	        StringValue (
	                    "ns3::ConstantRandomVariable[Constant=1.0]" ),
	        "Bounds",
	        RectangleValue (
	                    Rectangle ( 1125, 1175, 1321, 1371 ) ) );

   for ( uint16_t i = 0 ; i < ( ( ueNode.GetN ( ) * 50 ) / 100 ) ;
	        ++i )
   {    // 50% Mobile Users
      mobilityHelper.Install ( ueNode.Get ( i ) );    // User Equipment Lte Wlan
   }

// http://www.nsnam.org/doxygen-release/classns3_1_1_grid_position_allocator.html
// GRID POSITION ALLOCATIOR
   mobilityHelper.SetPositionAllocator ( "ns3::GridPositionAllocator",
	        "MinX", DoubleValue ( 1125 ), "MinY",
	        DoubleValue ( 1321 ), "DeltaX", DoubleValue ( 2 ),
	        "DeltaY", DoubleValue ( 2 ), "GridWidth",
	        UintegerValue ( 5 ), "LayoutType",
	        StringValue ( "RowFirst" ) );
   for ( uint16_t i = ( ( ueNode.GetN ( ) * 50 ) / 100 ) ;
	        i < ueNode.GetN ( ) ; ++i )
   {    // 50% Mobile Users
      mobilityHelper.Install ( ueNode.Get ( i ) );    // User Equipment Lte Wlan
   }

   Ptr < ListPositionAllocator > accessPointPosition = CreateObject <
	        ListPositionAllocator > ( );
   accessPointPosition->Add ( Vector ( 1150, 1346, 0 ) );
   mobilityHelper.SetPositionAllocator ( accessPointPosition );
   mobilityHelper.SetMobilityModel (
	        "ns3::ConstantPositionMobilityModel" );
   mobilityHelper.Install ( apNode );    // Access point

// Config::Connect ("/NodeList/*/$ns3::MobilityModel/CourseChange",
//                        MakeCallback (&HomeAgent::CourseChange));

   InternetStackHelper internetStackHelper;
   internetStackHelper.Install ( haNode );
   internetStackHelper.Install ( apNode );
   internetStackHelper.Install ( ueNode );

   enbDevice.Add ( lteHelper->InstallEnbDevice ( enbNode ) );    // eNB
   ueDeviceLte.Add ( lteHelper->InstallUeDevice ( ueNode ) );    // UE

   YansWifiChannelHelper wifiChannelHelper =
	        YansWifiChannelHelper::Default ( );
   Ptr < YansWifiChannel > channel = wifiChannelHelper.Create ( );
   wifiChannelHelper.SetPropagationDelay (
	        "ns3::ConstantSpeedPropagationDelayModel" );
   wifiChannelHelper.AddPropagationLoss (
	        "ns3::NakagamiPropagationLossModel", "Distance2",
	        DoubleValue ( 350.0 ) );

//Configuring the physical layer.
   YansWifiPhyHelper accessPointPhyHelper =
	        YansWifiPhyHelper::Default ( );
   accessPointPhyHelper.SetChannel ( channel );
   accessPointPhyHelper.Set ( "TxPowerStart", DoubleValue ( 16 ) );
   accessPointPhyHelper.Set ( "TxPowerEnd", DoubleValue ( 16 ) );
   accessPointPhyHelper.Set ( "TxPowerLevels", UintegerValue ( 1 ) );
   accessPointPhyHelper.Set ( "TxGain", DoubleValue ( 7.5 ) );
   accessPointPhyHelper.Set ( "RxGain", DoubleValue ( 7.5 ) );
   accessPointPhyHelper.SetErrorRateModel (
	        "ns3::NistErrorRateModel" );

   WifiHelper wifi = WifiHelper::Default ( );
   wifi.SetStandard ( WIFI_PHY_STANDARD_80211g );    // 802.11 G
   wifi.SetRemoteStationManager ( "ns3::ArfWifiManager" );
   Config::SetDefault (
	        "ns3::WifiRemoteStationManager::NonUnicastMode",
	        StringValue ( "DsssRate1Mbps" ) );
   wifi.SetRemoteStationManager ( "ns3::ConstantRateWifiManager",
	        "DataMode", StringValue ( "DsssRate1Mbps" ),
	        "ControlMode", StringValue ( "DsssRate1Mbps" ) );

   Ssid ssidAp = Ssid ( "WifiLocal" );

//MAC Layer
   NqosWifiMacHelper nqosWifiMacHelper =
	        NqosWifiMacHelper::Default ( );
   NqosWifiMacHelper accessPointMacHelper =
	        NqosWifiMacHelper::Default ( );
   nqosWifiMacHelper.SetType ( "ns3::StaWifiMac", "Ssid",
	        SsidValue ( ssidAp ), "ActiveProbing",
	        BooleanValue ( false ) );
   accessPointMacHelper.SetType ( "ns3::ApWifiMac", "Ssid",
	        SsidValue ( ssidAp ) );

   ueDeviceWlan.Add (
	        wifi.Install ( accessPointPhyHelper, nqosWifiMacHelper,
	                    ueNode ) );
   apDevice.Add (
	        wifi.Install ( accessPointPhyHelper, nqosWifiMacHelper,
	                    apNode ) );

   PointToPointHelper pppHelper;
   pppHelper.SetDeviceAttribute ( "DataRate",
	        DataRateValue ( DataRate ( "100Mb/s" ) ) );

// Server to PGW
   NetDeviceContainer haToPgw = pppHelper.Install ( pgwNode,
	        haNode.Get ( 0 ) );
   haDevice.Add ( haToPgw.Get ( 1 ) );    // Verificar metodo Install point-to-point-helper.cc.
// Server to AP
   NetDeviceContainer haToAp = pppHelper.Install ( apNode,
	        haNode.Get ( 0 ) );
   haDevice.Add ( haToAp.Get ( 1 ) );    // Verificar metodo Install point-to-point-helper.cc.
   apDevice.Add ( haToAp.Get ( 0 ) );

   Ipv4AddressHelper ipv4Helper;

// server and Pgw
   ipv4Helper.SetBase ( "192.168.1.0", "255.255.255.0" );    // setting up the IP base, this simulation needs to be Ipv4 because the NS3 have no support to IPv6 yet.
   Ipv4InterfaceContainer haToPgwInterface = ipv4Helper.Assign (
	        haToPgw );    // Creating a simple connection between PGW and the server (HA), this method assign set the indicated IP to the nodes.
   haInterface.Add ( haToPgwInterface.Get ( 1 ) );

// Server and AP
   ipv4Helper.SetBase ( "192.168.2.0", "255.255.255.0" );
   Ipv4InterfaceContainer haToApInterface = ipv4Helper.Assign (
	        haToAp );
   haInterface.Add ( haToApInterface.Get ( 1 ) );
   apInterface.Add ( haToApInterface.Get ( 0 ) );

// Access Point
   ipv4Helper.SetBase ( "11.0.0.0", "255.0.0.0" );
   apInterface.Add ( ipv4Helper.Assign ( apDevice ) );

// Ue Wifi
   ueInterface.Add ( ipv4Helper.Assign ( ueDeviceWlan ) );

// Ue Lte
   ueInterface.Add ( epcHelper->AssignUeIpv4Address ( ueDeviceLte ) );

// Routing Table
//Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
   Ipv4StaticRoutingHelper ipv4StaticRoutingHelper;

// Home Agent
   Ptr < Ipv4StaticRouting > haStaticRouting = ipv4StaticRoutingHelper
	        .GetStaticRouting (
	        haNode.Get ( 0 )->GetObject < Ipv4 > ( ) );
   haStaticRouting->AddNetworkRouteTo ( Ipv4Address ( "7.0.0.0" ),
	        Ipv4Mask ( "255.0.0.0" ), 1 );
   haStaticRouting->AddNetworkRouteTo ( Ipv4Address ( "11.0.0.0" ),
	        Ipv4Mask ( "255.0.0.0" ), 2 );

   Ptr < Ipv4StaticRouting > apStaticRouting = ipv4StaticRoutingHelper
	        .GetStaticRouting ( apNode->GetObject < Ipv4 > ( ) );
   apStaticRouting->AddNetworkRouteTo ( Ipv4Address ( "7.0.0.0" ),
	        Ipv4Mask ( "255.0.0.0" ), 2 );    // To Lte
   apStaticRouting->AddNetworkRouteTo ( Ipv4Address ( "192.168.1.0" ),
	        Ipv4Mask ( "255.255.255.0" ), 2 );    // The network between the Server (HA) and the PGW node

   for ( uint16_t c = 0 ; c < ueNode.GetN ( ) ; c++ )
   {
      Ptr < Ipv4StaticRouting > ueStaticRouting =
	           ipv4StaticRoutingHelper.GetStaticRouting (
	                       ueNode.Get ( c )->GetObject < Ipv4 > ( ) );
      // Default route WLAN
      ueStaticRouting->SetDefaultRoute ( Ipv4Address ( "11.0.0.1" ),
	           2 );
      // Default route LTE
      ueStaticRouting->SetDefaultRoute (
	           epcHelper->GetUeDefaultGatewayAddress ( ), 1 );
      // Ap to Server (HA)
      ueStaticRouting->AddNetworkRouteTo (
	           Ipv4Address ( "192.168.2.0" ),
	           Ipv4Mask ( "255.255.255.0" ),
	           Ipv4Address ( "11.0.0.1" ), 2 );
   }

   lteHelper->Attach ( ueDeviceLte, enbDevice.Get ( 0 ) );
   lteHelper->EnableTraces ( );
}

