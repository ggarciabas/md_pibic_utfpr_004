#!/bin/bash

# ---

../../waf --run "algoritmo 30 600 2700" >& 2700/log.600

# ---

../../waf --run "algoritmo 30 500 2700" >& 2700/log.500

# --- 

../../waf --run "algoritmo 30 400 2700" >& 2700/log.400

# --- 

../../waf --run "algoritmo 30 300 2700" >& 2700/log.300

# ---

../../waf --run "algoritmo 30 200 2700" >& 2700/log.200

# ---

../../waf --run "algoritmo 30 100 2700" >& 2700/log.100