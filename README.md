
* * *

## Documentação referente ao PIBIC realizado na UTFPR no período de 01/09/2012 à 31/08/2013.

* * *

`Título do projeto:` Implementação de um Controle de Admissão de Chamadas com prioridade em aplicações 4G utilizando o Network Simulator 3

`Título do trabalho:` Implementação de um Controle de Admissão de Chamadas utilizando o Network Simulator 3

`Aluna:` Giovanna Garcia Basilio <ggarciabas[at]gmail.com> 

`Orientador:` Hermes Irineu Del Monego <hmonego[at]utfpr.edu.br>

* * *

`Resumo:`

> No desenvolvimento deste trabalho foi construído um algoritmo para implementação de um Controle de Admissão de Chamadas (CAC) utilizando o simulador de redes de código aberto Network Simulator 3. Este algoritmo foi implementado em linguagem c++ utilizando as bibliotecas que o simulador disponibiliza para a criação dos nós (dispositivos) e configuração da rede. Para a construção do cenário de simulação foram utilizadas as tecnologias Long Term Evolution (LTE) e Wireless Local Area Network (WLAN), juntamente com operador de chamadas (Home Agent - HA), responsável por gerenciar as chamadas, e alguns dispositivos móveis com acesso as duas tecnologias. Quando o HA recebe uma requisição de chamada de um dos dispositivos de sua rede, ele verifica o status desta chamada e a tendência que ela tem de ser executada na inteface LTE e WLAN, esta tendência é definida com base no tipo de aplicação que esta sendo solicitada pelo usuário; caso não exista recursos suficientes para que uma chamada seja executada, o HA utiliza mecanismos de renegocição da mesma chamada ou realocação das chamadas já existentes. 
>
> Os resultados foram recolhidos a partir da execução deste algoritmo utilizando 2700 aplicações e uma faixa de 100 à 600 usuários na rede. Após a execução desta simulação foi possível visualizar que conforme maior número de usuários se conecta nas redes do HA, um menor número de chamadas são aceitas em suas interfaces padrão. Ocorrendo devido ao número de requisições em um cursto período de tempo, por conta da quantidade de usuários conectados, fazendo com que as chamadas sejam realocadas para outra interface (handover vertical). Com isso é possível concluir que um CAC aumenta a utilização dos recursos nas duas interfaces e também facilita a realocação das chamadas de uma interface para outra, permitindo assim, que uma chamada seja aceita.